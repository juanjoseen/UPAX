//
//  TextFieldExtension.swift
//  TripPlanner
//
//  Created by Juan Jose Elias Navaro on 08/06/21.
//

import UIKit

extension UITextField {
    func isValidEmail() -> Bool {
        if self.text != nil && self.text != "" {
            let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
            
            return testRegex(emailRegEx, with: self.text!)
        }
        return false
    }
    
    func isValidAlphabeticString() -> Bool {
        if self.text != nil && self.text != "" {
            let alphaRegEx = "^[a-zA-Z ]*$"
            return testRegex(alphaRegEx, with: self.text!)
        }
        return false
    }
    
    func isValidNumber() -> Bool {
        if self.text != nil && self.text != "" {
            let numberRegEx = "[-+]?([0-9]*\\.[0-9]+|[0-9]+)"
            return testRegex(numberRegEx, with: self.text!)
        }
        return false
    }
    
    private func testRegex(_ regEx:String, with string:String) -> Bool {
        let test = NSPredicate(format:"SELF MATCHES %@", regEx)
        return test.evaluate(with: string)
    }
}
