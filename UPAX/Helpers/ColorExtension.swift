//
//  ColorExtension.swift
//  Arctic Food
//
//  Created by Juan Jose Elias Navarro on 19/10/21.
//

import UIKit

extension UIColor {
    static var textColor: UIColor {
        return UIColor(named: "textColor") ?? UIColor(red: 0.25, green: 0.31, blue: 0.44, alpha: 1.00)
    }
    
    static var lineColor: UIColor {
        return UIColor(named: "lineColor") ?? UIColor(white: 0.0, alpha: 0.75)
    }
    
    static var defaultBackground: UIColor {
        return UIColor(named: "backgroundColor") ?? UIColor(red: 0.051, green: 0.051, blue: 0.127, alpha: 1.0)
    }
    
    static func hexColor(_ hex: String) -> UIColor {
        var cString: String = hex.trimmingCharacters(in: .whitespacesAndNewlines)

        if cString.hasPrefix("#") {
            cString.remove(at: cString.startIndex)
        }

        var rgbValue: UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255,
            blue: CGFloat(rgbValue & 0x0000FF) / 255,
            alpha: CGFloat(1.0)
        )
    }
}
