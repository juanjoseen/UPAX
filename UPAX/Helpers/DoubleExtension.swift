//
//  DoubleExtension.swift
//  Close2U
//
//  Created by Juan Jose Elias Navaro on 01/06/21.
//

import UIKit

extension Double {
    
    func toDeg() -> Double {
        return (self * 180.0) / .pi
    }
}
