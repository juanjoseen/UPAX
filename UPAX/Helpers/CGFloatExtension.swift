//
//  CGFloatExtension.swift
//  Arctic Food
//
//  Created by Juan Jose Elias Navarro on 19/10/21.
//

import UIKit

extension CGFloat {
    static var padding: CGFloat = 16.0
    static var inversePadding: CGFloat = -16.0
    static var lineHeight: CGFloat = 1.0
}
