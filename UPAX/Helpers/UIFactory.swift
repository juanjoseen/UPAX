//
//  UIFactory.swift
//  Arctic Food
//
//  Created by Juan Jose Elias Navarro on 19/10/21.
//

import UIKit

/**
 A helper for UI elements.
 
 - Author: Juan Jose Elias Navarro.
 
 - Note: All the methods are statics and is **not** necesary initilize and object for this class.
 */
class UIFactory {
    
    static func getImageView(with image: UIImage? = nil, contentMode mode: UIView.ContentMode = .scaleAspectFit) -> UIImageView {
        let imgView:UIImageView = UIImageView()
        imgView.translatesAutoresizingMaskIntoConstraints = false
        imgView.backgroundColor = .clear
        imgView.clipsToBounds = true
        imgView.contentMode = mode
        imgView.image = image
        
        return imgView
    }
    
    static func getView(backgroundColor: UIColor = .clear, radius: Int = 0, interactive: Bool = true) -> UIView {
        let view:UIView = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = backgroundColor
        view.isUserInteractionEnabled = interactive
        view.layer.cornerRadius = CGFloat(radius)
        return view
    }
    
    static func getLabel(text: String? = nil, color: UIColor = .textColor, font: UIFont = .normal(15), alignment: NSTextAlignment = .left) -> UILabel {
        let label:UILabel = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontSizeToFitWidth = true
        label.textAlignment = alignment
        label.textColor = color
        label.font = font
        label.text = text
        label.numberOfLines = 0
        return label
    }
    
    static func getTextField(placeHolder: String = "", font: UIFont = .normal(15), alignment: NSTextAlignment = .left, bottomLine: Bool = true) -> UITextField {
        let txtField:UITextField = UITextField()
        txtField.translatesAutoresizingMaskIntoConstraints = false
        txtField.placeholder = placeHolder
        txtField.font = font
        txtField.textAlignment = alignment
        
        if bottomLine {
            let line: UIView = UIFactory.getView(backgroundColor: .textColor)
            txtField.addSubview(line)
            
            NSLayoutConstraint.activate([
                line.heightAnchor.constraint(equalToConstant: .lineHeight),
                line.bottomAnchor.constraint(equalTo: txtField.bottomAnchor),
                line.leadingAnchor.constraint(equalTo: txtField.leadingAnchor),
                line.trailingAnchor.constraint(equalTo: txtField.trailingAnchor),
            ])
        }
        
        return txtField
    }
    
    static func getEmailTextField(placeHolder: String, font: UIFont = .text(), leftImage: UIImageView? = nil, bottomLine: Bool = true) -> UITextField {
        let txtField: UITextField = UITextField()
        txtField.translatesAutoresizingMaskIntoConstraints = false
        txtField.placeholder = placeHolder
        txtField.font = font
        txtField.textAlignment = .left
        txtField.keyboardType = .emailAddress
        txtField.autocapitalizationType = .none
        txtField.autocorrectionType = .no
        txtField.enablesReturnKeyAutomatically = true
        txtField.returnKeyType = .next
        txtField.spellCheckingType = .no
        if leftImage != nil {
            txtField.leftView = leftImage!
            txtField.leftViewMode = .always
        }
        
        if bottomLine {
            let line: UIView = UIFactory.getView(backgroundColor: .textColor)
            txtField.addSubview(line)
            
            NSLayoutConstraint.activate([
                line.heightAnchor.constraint(equalToConstant: .lineHeight),
                line.bottomAnchor.constraint(equalTo: txtField.bottomAnchor),
                line.leadingAnchor.constraint(equalTo: txtField.leadingAnchor),
                line.trailingAnchor.constraint(equalTo: txtField.trailingAnchor),
            ])
        }
        return txtField
    }
    
    static func getPasswordTextField(placeHolder: String, font: UIFont = .text(), leftImage: UIImageView? = nil) -> UITextField {
        let txtField: UITextField = UITextField()
        txtField.translatesAutoresizingMaskIntoConstraints = false
        txtField.placeholder = placeHolder
        txtField.font = font
        txtField.textAlignment = .left
        txtField.autocapitalizationType = .none
        txtField.autocorrectionType = .no
        txtField.enablesReturnKeyAutomatically = true
        txtField.returnKeyType = .done
        txtField.spellCheckingType = .no
        txtField.isSecureTextEntry = true
        if leftImage != nil {
            txtField.leftView = leftImage!
            txtField.leftViewMode = .always
        }
        
        let line: UIView = UIFactory.getView(backgroundColor: .textColor)
        txtField.addSubview(line)
        
        NSLayoutConstraint.activate([
            line.heightAnchor.constraint(equalToConstant: .lineHeight),
            line.bottomAnchor.constraint(equalTo: txtField.bottomAnchor),
            line.leadingAnchor.constraint(equalTo: txtField.leadingAnchor),
            line.trailingAnchor.constraint(equalTo: txtField.trailingAnchor),
        ])
        return txtField
    }
    
    static func getButton(image: UIImage? = nil, backgroundColor: UIColor = .clear, animated: Bool = true, interactive: Bool = true) -> UIButton {
        let button:UIButton = UIButton(frame: .zero)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.isUserInteractionEnabled = interactive
        button.backgroundColor = backgroundColor
        button.imageView?.contentMode = .scaleAspectFit
        button.imageView?.clipsToBounds = true
        button.clipsToBounds = true
        
        if image != nil {
            button.setImage(image, for: .normal)
        }
        
        if animated {
            button.addTarget(self, action: #selector(animatePressDown(_:)), for: .touchDown)
            button.addTarget(self, action: #selector(animatePressUp(_:)), for: .touchUpInside)
            button.addTarget(self, action: #selector(animatePressUp(_:)), for: .touchUpOutside)
            button.addTarget(self, action: #selector(animatePressUp(_:)), for: .touchCancel)
        }
        return button
    }
    
    static func getButton(text:String = "", textColor: UIColor = .textColor, font: UIFont = .medium(17), backgroundColor: UIColor = .clear, animated: Bool = true, interactive: Bool = true) -> UIButton {
        let button:UIButton = UIButton(frame: .zero)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.isUserInteractionEnabled = interactive
        button.backgroundColor = backgroundColor
        button.clipsToBounds = true
        button.setTitle(text, for: .normal)
        button.setTitleColor(textColor, for: .normal)
        button.titleLabel?.font = font
        
        if animated {
            button.addTarget(self, action: #selector(animatePressDown(_:)), for: .touchDown)
            button.addTarget(self, action: #selector(animatePressUp(_:)), for: .touchUpInside)
            button.addTarget(self, action: #selector(animatePressUp(_:)), for: .touchUpOutside)
            button.addTarget(self, action: #selector(animatePressUp(_:)), for: .touchCancel)
        }
        
        return button
    }
    
    static func getCollectionLayout(direction: UICollectionView.ScrollDirection = .vertical, itemSpacing: CGFloat = 0, lineSpacing: CGFloat = 0, headerSize: CGSize = .zero, footerSize: CGSize = .zero, inset: UIEdgeInsets = .zero) -> UICollectionViewFlowLayout {
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = itemSpacing
        layout.footerReferenceSize = footerSize
        layout.headerReferenceSize = headerSize
        layout.minimumLineSpacing = lineSpacing
        layout.scrollDirection = direction
        layout.sectionInset = inset
        return layout
    }
    
    static func getCollectionView(layout: UICollectionViewFlowLayout? = nil, backgroundColor: UIColor = .clear) -> UICollectionView {
        let collecion:UICollectionView = UICollectionView(frame: .zero, collectionViewLayout: layout ?? getCollectionLayout())
        collecion.translatesAutoresizingMaskIntoConstraints = false
        collecion.backgroundColor = backgroundColor
        
        return collecion
    }
}

// MARK: - Private Methods -
private extension UIFactory {
    
    // MARK: Animations
    @objc static func animatePressDown(_ sender: UIView) {
        ASAnimator.animatePressDown(sender)
    }

    @objc static func animatePressUp(_ sender: UIView) {
        ASAnimator.animatePressUp(sender)
    }
}
