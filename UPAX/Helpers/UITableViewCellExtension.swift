//
//  UITableViewCellExtension.swift
//  UPAX
//
//  Created by Juan Jose Elias Navarro on 19/12/21.
//

import UIKit

extension UITableViewCell {
    class func reusableIdentifier() -> String {
        let classType: AnyClass = object_getClass(self)!
        return NSStringFromClass(classType)
    }
}
