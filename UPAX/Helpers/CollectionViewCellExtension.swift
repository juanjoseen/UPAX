//
//  CollectionViewCellExtension.swift
//  SimpleStock
//
//  Created by Juan Jose Elias Navaro on 03/02/21.
//

import UIKit

extension UICollectionReusableView {
    
    /// Returns the parent collection view of the receiver.
    var parentCollectionView: UICollectionView? {
        var parent: UIView? = self.superview
        
        while parent != nil {
            if parent is UICollectionView {
                break
            }
            parent = parent?.superview
        }

        return parent as? UICollectionView
    }
    
    class func reusableIdentifier() -> String {
        let classType: AnyClass = object_getClass(self)!
        return NSStringFromClass(classType)
    }
    
}

extension UICollectionView {
    
    /// The type of animation to use when item are animated.
    enum CellAnimation {
        /// Item slide in from the bottom.
        case bottom
        /// Item slide in from the left.
        case left
        /// Item slide in from the right.
        case right
        /// Item slide in from the top.
        case top
        
        func springVelocity(_ collection: UICollectionView) -> CGFloat {
            let size = collection.superview?.bounds.size ?? .zero
            switch self {
                case .bottom, .top: return collection.bounds.height / size.height
                case .left, .right: return collection.bounds.width / size.width
            }
        }
        
        func translation(_ collection: UICollectionView) -> CGAffineTransform {
            let size = collection.superview?.bounds.size ?? .zero
            switch self {
                case .bottom: return CGAffineTransform(translationX: 0, y: size.height)
                case .left: return CGAffineTransform(translationX: -size.width, y: 0)
                case .right: return CGAffineTransform(translationX: size.width, y: 0)
                case .top: return CGAffineTransform(translationX: 0, y: -size.height)
            }
        }
    }
    
    /// Reloads all data of the receiver with a completion block to execute post-reload logic.
    func reloadData(completion: @escaping () -> ()) {
        UIView.animate(withDuration: 0, animations: {
            self.reloadData()
        }) { _ in
            completion()
        }
    }

    /// Animates the given cells (or all visibles) with the corresponding animation.
    ///
    /// If the `indexPaths` parameter is `nil`, this method animates all the visible cells of the receiver.
    ///
    /// - Parameters:
    ///     - indexPaths: An array of `IndexPath` which will be animated. If `nil`, all the visible cells will be animated.
    ///     - animation: A `CellAnimation` case that specifies the kind of animation to perform.
    ///     - fading: A Boolean value indicating if cells should fade while animating. The default value is `false`.
    ///     - completion: A block object to be executed when the animation sequence ends for each cell. This block has no return value and takes a single Boolean argument that indicates whether or not the animations actually finished before the completion handler was called. If the duration of the animation is 0, this block is performed at the beginning of the next run loop cycle. This parameter may be `nil`.
    func animateCells(at indexPaths: [IndexPath]? = nil, animation: CellAnimation, fading: Bool = false, completion: ((Bool) -> Void)? = nil) {
        let indexPathsForAnimate = indexPaths ?? indexPathsForVisibleItems
        
        let transform = animation.translation(self)
        let velocity = animation.springVelocity(self)
        for (n, indexPath) in indexPathsForAnimate.sorted(by: <).enumerated() {
            guard let cell = cellForItem(at: indexPath) else { continue }
            
            if fading {
                cell.alpha = 0
            }
            cell.transform = transform
            
            UIView.animate(withDuration: 1.2,
                           delay: Double(n) * 0.1,
                           usingSpringWithDamping: 0.93,
                           initialSpringVelocity: velocity,
                           options: [],
                animations: {
                    cell.alpha = 1
                    cell.transform = .identity
            }, completion: completion)
        }
    }
    
}
