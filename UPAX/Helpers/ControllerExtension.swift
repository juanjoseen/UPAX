//
//  ControllerExtension.swift
//  TripPlanner
//
//  Created by Juan Jose Elias Navaro on 08/06/21.
//

import UIKit

extension UIViewController {
    
    func push(_ controller: UIViewController, animated: Bool = true) {
        self.navigationController?.pushViewController(controller, animated: animated)
    }
    
    func pop(animated: Bool = true) {
        self.navigationController?.popViewController(animated: animated)
    }
    
    func addBackFromEdge() {
        let edge: UIScreenEdgePanGestureRecognizer = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(swipeFrromEdge(_:)))
        edge.edges = .left
        
        self.view.addGestureRecognizer(edge)
    }
    
    @objc private func swipeFrromEdge(_ sender: UIScreenEdgePanGestureRecognizer) {
        self.pop()
    }
}
