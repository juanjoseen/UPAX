//
//  SelfieModel.swift
//  UPAX
//
//  Created by Juan Jose Elias Navarro on 19/12/21.
//

import UIKit

class SelfieModel: BaseModel {
    init() {
        super.init(reusableIdentifier: SelfieCell.reusableIdentifier())
    }
}
