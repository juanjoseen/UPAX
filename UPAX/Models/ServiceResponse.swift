//
//  ServiceResponse.swift
//  UPAX
//
//  Created by Juan Jose Elias Navarro on 19/12/21.
//

import Foundation

struct ServiceResponse: Codable {
    let colors: [String]
    let questions: [Question]
}

struct Question: Codable {
    let total: Int
    let text: String
    let chartData: [ChartData]
}

struct ChartData: Codable {
    let text: String
    let percetnage: Int
}
