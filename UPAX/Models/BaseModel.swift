//
//  BaseModel.swift
//  UPAX
//
//  Created by Juan Jose Elias Navarro on 19/12/21.
//

import UIKit

class BaseModel {
    let reusableIdentifier: String
    
    init(reusableIdentifier: String) {
        self.reusableIdentifier = reusableIdentifier
    }
}
