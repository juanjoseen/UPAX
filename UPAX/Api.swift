//
//  Api.swift
//  UPAX
//
//  Created by Juan Jose Elias Navarro on 19/12/21.
//

import Foundation

class Api {
    private init() {}
    
    static let shared: Api = Api()
    
    func getChartInfo(completion: @escaping (ServiceResponse) -> Void) {
        guard let url: URL = URL(string: "https://us-central1-bibliotecadecontenido.cloudfunctions.net/helloWorld") else {
            print("No URL")
            return
        }
        URLSession.shared.dataTask(with: url) { data, response, error in
            if error == nil {
                do {
                    if let data = data {
                        let result: ServiceResponse = try JSONDecoder().decode(ServiceResponse.self, from: data)
                        completion(result)
                    } else {
                        print("No data")
                    }
                } catch {
                    print("Error parsing data: \(error)")
                }
            } else {
                print("Error getting data: \(error!)")
            }
        }.resume()
    }
}
