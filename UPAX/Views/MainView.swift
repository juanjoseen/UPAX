//
//  MainView.swift
//  UPAX
//
//  Created by Juan Jose Elias Navarro on 19/12/21.
//

import UIKit

class MainView: UIView {
    
    unowned var controller: ViewController!
    
    weak var tableView: UITableView?
    
    init(controller: ViewController) {
        super.init(frame: .zero)
        self.controller = controller
        
        backgroundColor = .defaultBackground
        
        let tableView = UITableView(frame: .zero)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = .defaultBackground
        
        tableView.register(TextCell.self, forCellReuseIdentifier: TextCell.reusableIdentifier())
        tableView.register(SelfieCell.self, forCellReuseIdentifier: SelfieCell.reusableIdentifier())
        tableView.register(DescriptionCell.self, forCellReuseIdentifier: DescriptionCell.reusableIdentifier())
        self.tableView = tableView
        
        addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: trailingAnchor),
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func showRetakeAlert() {
        let alert: UIAlertController = UIAlertController(title: "Que deseas hacer?", message: nil, preferredStyle: .actionSheet)
        
        let showSelfie: UIAlertAction = UIAlertAction(title: "Visualizar Selfie", style: .default) { action in
            self.controller.showSelfie()
        }
        
        let retakeSelfie: UIAlertAction = UIAlertAction(title: "Retomar Selfie", style: .default) { action in
            self.controller.takeSelfie()
        }
        
        let cancel: UIAlertAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        
        alert.addAction(showSelfie)
        alert.addAction(retakeSelfie)
        alert.addAction(cancel)
        
        controller.present(alert, animated: true, completion: nil)
    }
}

extension MainView: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        endEditing(true)
    }
    
}

extension MainView: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return controller.models.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model: BaseModel = controller.models[indexPath.row]
        let cell: BaseCell = tableView.dequeueReusableCell(withIdentifier: model.reusableIdentifier, for: indexPath) as! BaseCell
        cell.config(controller: controller)
        
        return cell
    }
    
    
}
