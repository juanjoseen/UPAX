//
//  ChartView.swift
//  UPAX
//
//  Created by Juan Jose Elias Navarro on 19/12/21.
//

import UIKit

class ChartView: UIView {

    unowned var controller: ChartViewController!
    
    private weak var tableView: UITableView?
    
    init(controller: ChartViewController) {
        self.controller = controller
        super.init(frame: .zero)
        
        backgroundColor = .defaultBackground
        
        let tableView = UITableView(frame: .zero)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = .defaultBackground
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(QuestionCell.self, forCellReuseIdentifier: QuestionCell.reusableIdentifier())
        self.tableView = tableView
        
        addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: topAnchor),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: trailingAnchor),
            
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func reloadData() {
        DispatchQueue.main.async {
            self.tableView?.reloadData()
        }
    }
    
}

extension ChartView: UITableViewDelegate {
    
}

extension ChartView: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("controller.questions.count: \(controller.questions.count)")
        return controller.questions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model: Question = controller.questions[indexPath.row]
        let cell: QuestionCell = tableView.dequeueReusableCell(withIdentifier: QuestionCell.reusableIdentifier(), for: indexPath) as! QuestionCell
        cell.config(controller: controller, model: model)
        return cell
    }
}
