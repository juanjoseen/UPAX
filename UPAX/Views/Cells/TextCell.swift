//
//  TextCell.swift
//  UPAX
//
//  Created by Juan Jose Elias Navarro on 19/12/21.
//

import UIKit

class TextCell: BaseCell {
    
    private weak var txtName: UITextField?

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let txtName: UITextField = UIFactory.getTextField(placeHolder: "Coloca tu nombre aqui", font: .subtitle(), alignment: .center, bottomLine: true)
        txtName.delegate = self
        txtName.enablesReturnKeyAutomatically = true
        self.txtName = txtName
        
        contentView.addSubview(txtName)
        
        NSLayoutConstraint.activate([
            txtName.topAnchor.constraint(equalTo: contentView.topAnchor, constant: .padding),
            txtName.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: .padding / 2.0),
            txtName.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: .inversePadding / 2.0),
            txtName.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: .inversePadding / 2.0),
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func config(controller: ViewController) {
        super.config(controller: controller)
    }

}

extension TextCell: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if !textField.isValidAlphabeticString() {
            textField.text = nil
        }
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if !textField.isValidAlphabeticString() {
            textField.text = nil
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        if !textField.isValidAlphabeticString() {
            textField.text = nil
        }
        controller.name = textField.text ?? ""
    }
}
