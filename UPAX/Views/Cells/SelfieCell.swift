//
//  SelfieCell.swift
//  UPAX
//
//  Created by Juan Jose Elias Navarro on 19/12/21.
//

import UIKit

class SelfieCell: BaseCell {

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let lblText: UILabel = UIFactory.getLabel(text: "Tomarse una selfie", font: .title(), alignment: .center)
        
        contentView.addSubview(lblText)
        
        NSLayoutConstraint.activate([
            lblText.topAnchor.constraint(equalTo: contentView.topAnchor, constant: .padding),
            lblText.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: .padding),
            lblText.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: .inversePadding),
            lblText.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: .inversePadding)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func config(controller: ViewController) {
        super.config(controller: controller)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            controller.showMenuForSelfie()
        }
    }
    
}
