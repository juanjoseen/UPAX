//
//  DescriptionCell.swift
//  UPAX
//
//  Created by Juan Jose Elias Navarro on 19/12/21.
//

import UIKit

class DescriptionCell: BaseCell {

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let lblText: UILabel = UIFactory.getLabel(text: "Una gráfica o representación gráfica es un tipo de representación de datos, generalmente numéricos, mediante recursos visuales (líneas, vectores, superficies o símbolos), para que se manifieste visualmente la relación matemática o correlación estadística que guardan entre sí. También es el nombre de un conjunto de puntos que se plasman en coordenadas cartesianas y sirven para analizar el comportamiento de un proceso o un conjunto de elementos o signos que permiten la interpretación de un fenómeno. La representación gráfica permite establecer valores que no se han obtenido experimentalmente sino mediante la interpolación (lectura entre puntos) y la extrapolación (valores fuera del intervalo experimental).", font: .text(), alignment: .left)
        
        contentView.addSubview(lblText)
        
        NSLayoutConstraint.activate([
            lblText.topAnchor.constraint(equalTo: contentView.topAnchor, constant: .padding),
            lblText.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: .padding),
            lblText.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: .inversePadding),
            lblText.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: .inversePadding)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func config(controller: ViewController) {
        super.config(controller: controller)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            controller.showChart()
        }
    }

}
