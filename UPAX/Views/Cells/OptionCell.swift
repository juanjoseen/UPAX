//
//  OptionCell.swift
//  UPAX
//
//  Created by Juan Jose Elias Navarro on 19/12/21.
//

import UIKit

class OptionCell: UICollectionViewCell {
    
    weak var imgColor: UIView?
    weak var lblText: UILabel?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let imgColor: UIView = UIFactory.getView()
        imgColor.layer.cornerRadius = 10
        self.imgColor = imgColor
        
        let lblText: UILabel = UIFactory.getLabel(font: .text(), alignment: .left)
        self.lblText = lblText
        
        addSubview(imgColor)
        addSubview(lblText)
        
        NSLayoutConstraint.activate([
            imgColor.leadingAnchor.constraint(equalTo: leadingAnchor, constant: .padding),
            imgColor.widthAnchor.constraint(equalToConstant: 20),
            imgColor.heightAnchor.constraint(equalTo: imgColor.widthAnchor),
            imgColor.centerYAnchor.constraint(equalTo: centerYAnchor),
            
            lblText.leadingAnchor.constraint(equalTo: imgColor.trailingAnchor, constant: .padding / 2.0),
            lblText.trailingAnchor.constraint(equalTo: trailingAnchor, constant: .inversePadding),
            lblText.topAnchor.constraint(equalTo: imgColor.topAnchor),
            lblText.bottomAnchor.constraint(equalTo: imgColor.bottomAnchor)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func config(model: ChartData, color: UIColor) {
        imgColor?.backgroundColor = color
        lblText?.text = String(format: "%@ %d%%", model.text, model.percetnage)
    }
}
