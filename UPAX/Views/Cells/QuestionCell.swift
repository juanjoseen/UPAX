//
//  QuestionCell.swift
//  UPAX
//
//  Created by Juan Jose Elias Navarro on 19/12/21.
//

import UIKit

class QuestionCell: UITableViewCell {
    
    private weak var lblTitle: UILabel?
    private weak var collection: UICollectionView?
    
    private var controller: ChartViewController!
    private var model: Question!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let lblTitle: UILabel = UIFactory.getLabel(text: "hola mundo", color: .white, font: .title(), alignment: .center)
        self.lblTitle = lblTitle
        
        let collection: UICollectionView = UIFactory.getCollectionView()
        collection.register(OptionCell.self, forCellWithReuseIdentifier: OptionCell.reusableIdentifier())
        collection.delegate = self
        collection.dataSource = self
        self.collection = collection
        
        contentView.addSubview(lblTitle)
        contentView.addSubview(collection)
        
        NSLayoutConstraint.activate([
            lblTitle.topAnchor.constraint(equalTo: contentView.topAnchor, constant: .padding),
            lblTitle.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: .padding),
            lblTitle.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: .inversePadding),
            
            collection.topAnchor.constraint(equalTo: lblTitle.bottomAnchor, constant: .padding),
            collection.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            collection.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            collection.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: .inversePadding)
        ])
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func config(controller: ChartViewController, model: Question) {
        self.controller = controller
        self.model = model
        lblTitle?.text = model.text
        collection?.heightAnchor.constraint(equalToConstant: CGFloat(model.chartData.count)/2.0 * 50.0).isActive = true
        
    }
}

extension QuestionCell: UICollectionViewDelegate {
    
}

extension QuestionCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model.chartData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: OptionCell = collectionView.dequeueReusableCell(withReuseIdentifier: OptionCell.reusableIdentifier(), for: indexPath) as! OptionCell
        let model: ChartData = model.chartData[indexPath.row]
        cell.config(model: model, color: controller.colors[indexPath.row])
        return cell
    }
}

extension QuestionCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width: CGFloat = collectionView.frame.width / 2.0
        let height: CGFloat = 50
        return CGSize(width: width, height: height)
    }
}
