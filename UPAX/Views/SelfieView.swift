//
//  SelfieView.swift
//  UPAX
//
//  Created by Juan Jose Elias Navarro on 19/12/21.
//

import UIKit

class SelfieView: UIView {

    unowned var controller: SelfieViewController!
    
    weak var imgSelfie: UIImageView?
    
    init(controller: SelfieViewController) {
        self.controller = controller
        super.init(frame: .zero)
        
        backgroundColor = .defaultBackground
        
        let imgSelfie: UIImageView = UIFactory.getImageView(with: controller.selfie, contentMode: .scaleAspectFit)
        self.imgSelfie = imgSelfie
        
        let btnClose: UIButton = UIFactory.getButton(image: UIImage(systemName: "xmark.circle"))
        btnClose.tintColor = .systemBlue
        btnClose.addTarget(self, action: #selector(actionClose), for: .touchUpInside)
        
        addSubview(imgSelfie)
        addSubview(btnClose)
        
        NSLayoutConstraint.activate([
            imgSelfie.topAnchor.constraint(equalTo: topAnchor),
            imgSelfie.bottomAnchor.constraint(equalTo: bottomAnchor),
            imgSelfie.leadingAnchor.constraint(equalTo: leadingAnchor),
            imgSelfie.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            btnClose.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: .padding),
            btnClose.trailingAnchor.constraint(equalTo: trailingAnchor, constant: .inversePadding),
            btnClose.heightAnchor.constraint(equalTo: btnClose.widthAnchor),
            btnClose.widthAnchor.constraint(equalToConstant: 50),
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func actionClose() {
        self.controller.pop()
    }
    
}
