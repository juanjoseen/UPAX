//
//  ChartViewController.swift
//  UPAX
//
//  Created by Juan Jose Elias Navarro on 19/12/21.
//

import UIKit

class ChartViewController: UIViewController {
    
    var chartView: ChartView?
    
    var colors: [UIColor] = []
    var questions: [Question] = []
    
    override func loadView() {
        let chartView: ChartView = ChartView(controller: self)
        self.chartView = chartView
        view = chartView
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        addBackFromEdge()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Api.shared.getChartInfo { response in
            //print(response)
            self.colors = []
            for color in response.colors {
                self.colors.append(.hexColor(color))
            }
            self.questions = response.questions
            self.chartView?.reloadData()
        }
    }
}
