//
//  SelfieViewController.swift
//  UPAX
//
//  Created by Juan Jose Elias Navarro on 19/12/21.
//

import UIKit

class SelfieViewController: UIViewController {
    
    var viewSelfie: SelfieView?
    var selfie: UIImage!
    
    convenience init(selfie: UIImage) {
        self.init()
        self.selfie = selfie
    }
    
    override func loadView() {
        let viewSelfie: SelfieView = SelfieView(controller: self)
        self.viewSelfie = viewSelfie
        view = viewSelfie
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        addBackFromEdge()
    }

}
