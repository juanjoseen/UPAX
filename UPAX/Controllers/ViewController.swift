//
//  ViewController.swift
//  UPAX
//
//  Created by Juan Jose Elias Navarro on 19/12/21.
//

import UIKit

class ViewController: UIViewController {
    
    weak var main: MainView?
    
    var models: [BaseModel] = []
    
    var name: String = ""
    var selfie: UIImage? = nil
    
    override func loadView() {
        let main: MainView = MainView(controller: self)
        self.main = main
        view = main
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        models = [
            TextModel(),
            SelfieModel(),
            DescriptionModel()
        ]
    }
    
    func showMenuForSelfie() {
        if let _: UIImage = selfie {
            self.main?.showRetakeAlert()
        } else {
            takeSelfie()
        }
    }
    
    func showSelfie() {
        if let selfie = selfie {
            push(SelfieViewController(selfie: selfie))
        }
    }
    
    func takeSelfie() {
        let imgPicker: UIImagePickerController = UIImagePickerController()
        imgPicker.sourceType = .camera
        imgPicker.allowsEditing = true
        imgPicker.delegate = self
        present(imgPicker, animated: true, completion: nil)
    }
    
    func showChart() {
        push(ChartViewController())
    }

}

extension ViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        
        guard let image: UIImage = info[.editedImage] as? UIImage else {
            return
        }
        
        self.selfie = image
    }
}
